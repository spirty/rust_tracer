extern crate nalgebra as na;

pub trait Color {
    fn get_ambient(&self) -> na::Vector3<f64>;
    fn get_specular(&self) -> na::Vector3<f64>;
    fn get_diffuse(&self) -> na::Vector3<f64>;
    fn get_shininess(&self) -> f64;
}

pub fn new(x:u8,y:u8,z:u8) -> na::Vector3<f64>{
    na::Vector3::new((x as f64) / 255.0, (y as f64) / 255.0, (z as f64) / 255.0)
}

pub fn fin(col: na::Vector3<f64>) -> [u8; 3] {
    [(col.x * 255.0).floor() as u8,
     (col.y * 255.0).floor() as u8,
     (col.z * 255.0).floor() as u8]
}

pub fn get_ambient<T: Color>(shape: &T) -> na::Vector3<f64> {
    shape.get_ambient()
}

pub fn get_specular<T: Color>(shape: &T) -> na::Vector3<f64> {
    shape.get_specular()
}

pub fn get_diffuse<T: Color>(shape: &T) -> na::Vector3<f64> {
    shape.get_diffuse()
}

pub fn get_shininess<T: Color>(shape: &T) -> f64 {
    shape.get_shininess()
}
