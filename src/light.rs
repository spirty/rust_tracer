extern crate nalgebra as na;

pub enum Light  {
    DirLight {
        dir: na::Vector3<f64>,
        ambient: na::Vector3<f64>,
        specular: na::Vector3<f64>,
        diffuse: na::Vector3<f64>,
    },
    SpotLight {
        pos: na::Point3<f64>,
        ambient: na::Vector3<f64>,
        specular: na::Vector3<f64>,
        diffuse: na::Vector3<f64>,
    },
}

impl Light {
    pub fn get_pos(&self) -> na::Point3<f64> {
        match self {
            &Light::DirLight{dir: d, ..} => {d.to_point()}
            &Light::SpotLight{pos: p, ..} => {p}
        }
    }

    pub fn get_ambient(&self) -> na::Vector3<f64> {
        match self {
            &Light::DirLight{ambient: a , ..} => {a}
            &Light::SpotLight{ambient: a , ..} => {a}
        }
    }

    pub fn get_diffuse(&self) -> na::Vector3<f64> {
        match self {
            &Light::DirLight{diffuse: d, ..} => {d}
            &Light::SpotLight{diffuse: d, ..} => {d}
        }
    }

    pub fn get_specular(&self) -> na::Vector3<f64> {
        match self {
            &Light::DirLight{specular: s, ..} => {s}
            &Light::SpotLight{specular: s, ..} => {s}
        }
    }
}
