extern crate raytracer;

extern crate nalgebra as na;
extern crate image;

use std::fs::File;
use std::path::Path;
use na::Norm;
use na::Isometry3;
use na::Translate;
use na::Inverse;

use raytracer::shapes::*;
use raytracer::light::*;

static FOV: f64  = 60.0;
static IMGX: u32 = 400;
static IMGY: u32 = 400;

fn main () {
    print!("Rendering...\n");

    let mut b = Cube::new(na::Point3::new(0.0, 0.0, 0.0), 1.0, 1.0, 1.0);
    let mut b = Sphere::new(na::Point3::new(0.0, 0.0, 0.0), 1.0);

    b.ambient = raytracer::color::new(150, 150, 150);
    b.diffuse = raytracer::color::new(150, 150, 150);
    b.specular = raytracer::color::new(190, 190, 190);

    let mut imgbuf = image::ImageBuffer::new(IMGX, IMGY);
    let scale:f64 = (FOV * 0.5f64).to_radians().tan();
    let img_aspect_ratio:f64 = (IMGX as f64) / (IMGY as f64);

    let mut camera = na::Isometry3::look_at_rh(&na::Point3::new(0.0f64, 0.0, -3.0),
                                               &na::Point3::new(0.0f64, 0.0, 0.0),
                                               &na::Vector3::new(0.0f64, 1.0, 0.0));

    let mut lights:Vec<Light>  = Vec::new();

    lights.push(Light::DirLight{ dir: na::Vector3::new(1.0f64,0.0,1.0),
                                 ambient: raytracer::color::new(100,100,100),
                                 specular: raytracer::color::new(100,100,100),
                                 diffuse: raytracer::color::new(100,100,100),
    });

    print!("camera pos {}", camera);

    print!("dot {}\n", raytracer::util::geom::reflect(na::Vector3::new(1.0f64, 1.0, 0.0), na::Vector3::new(0.0f64, 1.0, 0.0)));

    for (x,y,pixel) in imgbuf.enumerate_pixels_mut() {

        // compute the ray direction
        let r_x:f64 = (2.0 * ((x as f64) + 0.5) / (IMGX as f64) - 1.0) * img_aspect_ratio * scale;
        let r_y:f64 = (1.0 - 2.0 * ((y as f64) + 0.5) / (IMGY as f64)) * scale;
        let dir = camera.inverse().unwrap() * na::Vector3::new(r_x, r_y, -1.0);
        let orig = na::Point3::new(0.0, 0.0, 0.0) - camera.translation * camera.rotation;
        if x == 200 && y == 200 {
            print!("Ray origin is at {} and direction is {}\n", orig, dir);
        }
        let ray = Ray::new(orig, dir.normalize());

        let pix = match intersect_shape(&b, &ray) {
            Some((i1,_))  => image::Rgb(compute_color(&b, &ray, i1, &lights)),
            None          => image::Rgb([0u8, 0u8, 0u8]),
        };

        *pixel = pix;
    }

    let ref mut fout = File::create(&Path::new("result.png")).unwrap();

    let _ = image::ImageRgb8(imgbuf).save(fout, image::PNG);

    print!("Done\n");
}
