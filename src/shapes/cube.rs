extern crate nalgebra as na;

use std::mem;
use shapes::ray::*;
use color::*;

pub struct Cube {
    min: na::Point3<f64>,
    max: na::Point3<f64>,
    pub ambient: na::Vector3<f64>,
    pub diffuse: na::Vector3<f64>,
    pub specular: na::Vector3<f64>,
    pub shininess: f64,
}

impl Cube {
    pub fn new (center: na::Point3<f64>, width: f64, height: f64, length: f64) -> Cube {
        let min = na::Point3::new (center.x - width / 2.0,
                                   center.y - height / 2.0,
                                   center.z - length / 2.0);
        let max = na::Point3::new (center.x + width / 2.0,
                                   center.y + height / 2.0,
                                   center.z + length / 2.0);
        Cube {min: min,
              max: max,
              ambient: na::Vector3::new(0.0, 0.0, 0.0),
              specular: na::Vector3::new(0.0, 0.0, 0.0),
              diffuse: na::Vector3::new(0.0, 0.0, 0.0),
              shininess: 100.0
        }
    }
}

impl Intersect for Cube {
    fn intersect (&self, ray: &Ray) -> Option<(f64, f64)> {
        let mut tmin = (self.min.x - ray.origin.x) / ray.dir.x;
        let mut tmax = (self.max.x - ray.origin.x) / ray.dir.x;

        if tmin > tmax { mem::swap (&mut tmin, &mut tmax); };

        let mut tymin = (self.min.y - ray.origin.y) / ray.dir.y;
        let mut tymax = (self.max.y - ray.origin.y) / ray.dir.y;

        if tymin > tymax { mem::swap (&mut tymin, &mut tymax); }

        if tmin > tymax || (tymin > tmax) { return None }

        if tymin > tmin { tmin = tymin; }

        if tymax < tmax { tmax = tymax; }

        let mut tzmin = (self.min.z - ray.origin.z) / ray.dir.z;
        let mut tzmax = (self.max.z - ray.origin.z) / ray.dir.z;

        if tzmin > tzmax { mem::swap (&mut tzmin, &mut tzmax); }

        if tmin > tzmax || (tzmin > tmax) { return None }

        if tzmin > tmin { tmin = tzmin; }

        if tzmax < tmax { tmax = tzmax; }

        if tmin.is_infinite() { return None }

        Some((tmin, tmax))
    }

    fn get_normal (&self, ray: &Ray, hit: f64) -> na::Vector3<f64> {
        let int_point = ray.origin + ray.dir * hit;
        let mut x_comp = 0.0f64;
        let mut y_comp = 0.0f64;
        let mut z_comp = 0.0f64;
        // TODO confirm this math
        if (int_point.x - self.min.x).abs() < 0.001f64 {
            x_comp = -1.0f64;
        }
        if (int_point.x - self.max.x).abs() < 0.001f64 {
            x_comp = 1.0f64;
        }
        if (int_point.y - self.min.y).abs() < 0.001f64 {
            y_comp = -1.0f64;
        }
        if (int_point.y - self.max.y).abs() < 0.001f64 {
            y_comp = 1.0f64;
        }
        if (int_point.z - self.min.z).abs() < 0.001f64 {
            z_comp = -1.0f64;
        }
        if (int_point.z - self.max.z).abs() < 0.001f64 {
            z_comp = 1.0f64;
        }
        na::Vector3::new(x_comp, y_comp, z_comp)
    }
}

impl Color for Cube {
    fn get_ambient(&self) -> na::Vector3<f64> {
        self.ambient
    }

    fn get_specular(&self) -> na::Vector3<f64> {
        self.specular
    }

    fn get_diffuse(&self) -> na::Vector3<f64> {
        self.diffuse
    }

    fn get_shininess(&self) -> f64 {
        self.shininess
    }
}
