extern crate nalgebra as na;

use light::*;
use color::*;
use util::*;
use std::cmp::PartialOrd;
use self::na::Norm;
use std::ops::{Neg, Mul};

pub struct Ray {
    pub origin: na::Point3<f64>,
    pub dir: na::Vector3<f64>,
}

impl Ray {
    pub fn new(or: na::Point3<f64>, dir: na::Vector3<f64>) -> Ray {
        Ray { origin: or,
              dir: dir,
        }
    }
}

pub trait Intersect {
    fn intersect (&self, ray: &Ray) -> Option<(f64, f64)>;
    fn get_normal (&self, ray: &Ray, hit: f64) -> na::Vector3<f64>;
}

pub fn intersect_shape<T: Intersect>(shape: &T, ray: &Ray) -> Option<(f64, f64)> {
    shape.intersect(ray)
}

pub fn compute_color<T: Intersect + Color> (shape: &T, ray: &Ray, hit: f64, lights: &Vec<Light>)
                                            -> [u8; 3] {
    //compute_color_normals(shape, ray, hit, lights)
    compute_color_phong(shape, ray, hit, lights)
}

fn compute_color_normals<T: Intersect + Color> (shape: &T, ray: &Ray, hit: f64, lights: &Vec<Light>)
                                                -> [u8; 3] {
    let N = shape.get_normal(&ray, hit);
    [((N.x + 1.0) * 127.0).floor() as u8,
     ((N.y + 1.0) * 127.0).floor() as u8,
     ((N.z + 1.0) * 127.0).floor() as u8]
}

// TODO implement properly
fn compute_color_phong<T: Intersect + Color> (shape: &T, ray: &Ray, hit: f64, lights: &Vec<Light>)
                                        -> [u8; 3] {
    let N = shape.get_normal(&ray, hit);
    let int_point = ray.origin + ray.dir * hit;
    let mut fin_color = na::Vector3::new(0.0, 0.0, 0.0);

    for l in lights {
        let L = match l {
            &Light::SpotLight{..} => l.get_pos() -  int_point,
            &Light::DirLight{..} => l.get_pos().to_vector().neg(),
        };
        let R = geom::reflect(L, N).neg().normalize();

        let n_dot_l = na::dot(&N, &L).max(0.0);
        let r_dot_v = na::dot(&R, &int_point.as_vector()).max(0.0).powf(shape.get_shininess());

        let amb = l.get_ambient() * shape.get_ambient();
        let diff = l.get_diffuse() * shape.get_diffuse() * na::Vector3::new(n_dot_l, n_dot_l, n_dot_l);
        let spec = l.get_specular() * shape.get_specular() * na::Vector3::new(r_dot_v, r_dot_v, r_dot_v);




        fin_color = amb +
            na::partial_clamp(&diff, &na::Vector3::new(0.0, 0.0, 0.0), &na::Vector3::new(1.0, 1.0, 1.0)).unwrap().clone() +
            na::partial_clamp(&spec, &na::Vector3::new(0.0, 0.0, 0.0), &na::Vector3::new(1.0, 1.0, 1.0)).unwrap().clone();
        // fin_color = na::partial_clamp(&spec, &na::Vector3::new(0.0, 0.0, 0.0), &na::Vector3::new(1.0, 1.0, 1.0)).unwrap().clone();
    }

    fin(fin_color)
}
