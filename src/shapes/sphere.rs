extern crate nalgebra as na;

use shapes::ray::*;
use color::*;

pub struct Sphere {
    center: na::Point3<f64>,
    radius: f64,
    radius2: f64,
    pub ambient: na::Vector3<f64>,
    pub diffuse: na::Vector3<f64>,
    pub specular: na::Vector3<f64>,
    pub shininess: f64,
}

impl Sphere {
    pub fn new (center: na::Point3<f64>, rad: f64) -> Sphere {
        Sphere {
            center: center,
            radius: rad,
            radius2: rad * rad,
            ambient: na::Vector3::new(0.0, 0.0, 0.0),
            specular: na::Vector3::new(0.0, 0.0, 0.0),
            diffuse: na::Vector3::new(0.0, 0.0, 0.0),
            shininess: 100.0,
        }
    }
}

impl Intersect for Sphere {
    fn intersect (&self, ray: &Ray) -> Option<(f64, f64)> {
        let l = self.center - ray.origin;
        let tca = na::dot(&l, &ray.dir);
        if tca < 0.0 { return None };

        let d2 = na::dot(&l, &l) - tca * tca;
        if d2 > self.radius2 {return None };

        // compute the hit pos
        let thc = f64::sqrt(self.radius2 - d2);
        // return same ray with a hit occunted for
        Some((tca - thc, tca + thc))
    }

    fn get_normal (&self, ray: &Ray, hit: f64) -> na::Vector3<f64> {
        let int_point = ray.origin + ray.dir * hit;
        let norm = int_point - self.center;
        na::normalize(&norm)
    }
}

impl Color for Sphere {
    fn get_ambient(&self) -> na::Vector3<f64> {
        self.ambient
    }

    fn get_specular(&self) -> na::Vector3<f64> {
        self.specular
    }

    fn get_diffuse(&self) -> na::Vector3<f64> {
        self.diffuse
    }

    fn get_shininess(&self) -> f64 {
        self.shininess
    }
}
