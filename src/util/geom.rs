extern crate nalgebra as na;

use std::ops::{Mul, Sub};
use self::na::Dot;

pub fn reflect(d: na::Vector3<f64>, n: na::Vector3<f64>) -> na::Vector3<f64> {
    //d - (d.dot(&n))*n*(na::Vector3::new(2.0, 2.0, 2.0))
    d - (na::dot(&d, &n) * n * (na::Vector3::new(2.0, 2.0, 2.0)))
}
