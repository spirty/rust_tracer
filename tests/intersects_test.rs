extern crate nalgebra as na;
extern crate raytracer;

use raytracer::shapes::*;

#[test]
fn test_box_intersect() {
    let b = Cube::new(na::Point3::new(0.0, 0.0, 0.0), 1.0, 1.0, 1.0);

    let r = Ray::new(na::Point3::new(-10.0, 0.0, 0.0),
                     na::Vector3::new(1.0, 0.0, 0.0));
    let point = intersect_shape(&b, &r);

    match point  {
        Some((x1, x2)) => assert!(x1 == 9.5 && x2 == 10.5, "Expected {} but got {} or {}", 9.5, x1, x2),
        None           => assert!(false, "Expected an intersection"),
    }
}

#[test]
fn test_sphere_intersect() {
    let b = Sphere::new(na::Point3::new(0.0, 0.0, 0.0), 0.5 );
    let r = Ray::new(na::Point3::new(0.0, 0.0, 5.0),
                     na::Vector3::new(1.0, 0.0, -1.0));
    let point = intersect_shape(&b, &r);

    match point  {
        Some((x1, x2)) => assert!(x1 == 4.5 || x2 == 5.5, "Expected {} but got {} or {}", 9.5, x1, x2),
        None           => assert!(false, "Expected an intersection"),
    }
}

#[test]
fn test_no_intersect() {
    let b = Cube::new(na::Point3::new(0.0, 0.0, 0.0), 1.0, 1.0, 1.0);

    let r = Ray::new(na::Point3::new(-10.0, 0.0, 0.0),
                     na::Vector3::new(0.0, 0.0, 1.0));
    let point = intersect_shape(&b, &r);

    match point  {
        Some((x1, x2)) => assert!(false, "Expected no intersect but got {} or {}", x1, x2),
        None           => assert!(true, "Expected no intersection"),
    }
}
