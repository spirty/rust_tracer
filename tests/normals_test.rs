extern crate nalgebra as na;
extern crate raytracer;

use raytracer::shapes::*;

#[test]
fn test_sphere_intersects() {
    let b = Sphere::new(na::Point3::new(0.0, 0.0, 0.0), 0.5 );
    let r = Ray::new(na::Point3::new(0.0, 0.0, 5.0),
                     na::Vector3::new(0.0, 0.0, -1.0));
    let point = intersect_shape(&b, &r);
    match point {
        Some((p1, _)) => {
            let norm = b.get_normal(&r, p1);
            assert!(norm == na::Vector3::new(0.0, 0.0, 1.0),
                    "expected normal vector {} got {}",
                    na::Vector3::new(0.0, 0.0, 1.0),
                    norm
            )
        },
        None          => assert!(false)
    }
}
