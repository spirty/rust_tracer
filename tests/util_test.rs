extern crate nalgebra as na;
extern crate raytracer;

use raytracer::util::*;

#[test]
fn reflect_test() {
    let v = na::Vector3::new(-1.0f64, 0.0, 0.0);
    let r = na::Vector3::new(1.0f64, 0.0, 0.0);
    let n = na::Vector3::new(0.0f64, 1.0, 0.0);

    assert_eq!(geom::reflect(v,n), r, "expected {}, got {}", r, geom::reflect(v,n));
}
